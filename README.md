# Flutter_Learning_Collection
Flutter学习记录

#### 基础
1. ListView相关
2. ScrollView相关
3. 事件处理与通知
4. 生命周期又是怎样的

#### 动画
1. Animation
2. CurvedAnimation
3. AnimationController
4. Tween

#### 网络与存储
1. HTTP编程
2. 本地存储
3. 数据库
4. JSON解析

#### 进阶
1. 状态管理有哪几种
2. 跨组件通信有几种方式
3. 国际化怎么操作
4. 线程模型（异步、并发、事件循环）
5. 屏幕适配方案

#### 疑问
1. GetX为什么能刷新Stateless
2. GetX为什么不需要context
