package com.example.flutter_application

import android.os.Build
import android.os.Bundle
import android.util.Log
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import io.flutter.embedding.android.FlutterActivity
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // 隐藏状态栏
        ImmersionBar.with(this).hideBar(BarHide.FLAG_HIDE_STATUS_BAR).init();
        Log.i("状态栏","隐藏")
        if (flutterEngine != null){
            val channel = MethodChannel(flutterEngine!!.dartExecutor, "channel.method.android")
            channel.setMethodCallHandler { call, result ->
                when (call.method) {
                    "showStatusBar" -> {
                        ImmersionBar.with(this).hideBar(BarHide.FLAG_SHOW_BAR).init();
                        result.success(true);
                        Log.i("状态栏",call.method)
                    }
                    else -> result.notImplemented()
                }
            }
        }
    }
}
