//
//  MethodChannelHandler.swift
//  Runner
//
//  Created by yuan xiaodong on 2023/6/7.
//


import Flutter
import UIKit

public class MethodChannelHandler{
    //通知flutter检测
    var appChannel:FlutterMethodChannel
    
    init(messenger: FlutterBinaryMessenger) {
        appChannel = FlutterMethodChannel(name: "channel.method", binaryMessenger: messenger)
        appChannel.setMethodCallHandler { (call:FlutterMethodCall, result:@escaping FlutterResult) in
            if (call.method == "appDidLoad") {
                self.appDidLoad();
            }
        }
    }
    
    //启动完成
    func appDidLoad(){
        print("App启动完成，删除启动动画")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "appDidLoad"), object: nil)
    }
}

