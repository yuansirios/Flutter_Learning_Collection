//
//  EnteranceController.swift
//  Runner
//
//  Created by yuan xiaodong on 2023/6/7.
//

import Foundation
import Flutter

class EnteranceController:FlutterViewController{
    var screenView : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(appDidLoad), name:  NSNotification.Name(rawValue: "appDidLoad"), object: nil);
        launchScreenAnimation()
    }
    
    @objc func appDidLoad() {
        print("App启动完成，删除启动动画")
        UIApplication.shared.setStatusBarHidden(false, with: .fade)
        UIApplication.shared.setStatusBarStyle(.default, animated: true)
        if (screenView != nil){
            UIView.animate(withDuration: 1,
                           delay: 0,
                           options: .curveEaseInOut,
                           animations: {
                self.screenView!.alpha = 0
            }){(finished) in
                self.screenView?.removeFromSuperview()
            }
        }
    }
    
    func launchScreenAnimation(){
        guard let launchScreen = UIStoryboard(name: "LaunchScreen", bundle: nil).instantiateInitialViewController() else {return}
        launchScreen.view.frame = self.view.bounds;
        self.view.addSubview(launchScreen.view)
        screenView = launchScreen.view;
        if let label = launchScreen.view.viewWithTag(1) as? UILabel {
            UIView.animate(withDuration: 1,
                           delay: 0,
                           options: [.repeat,.autoreverse],
                           animations: {
                label.alpha = 0.3
            }){(finished) in
                label.alpha = 1
            }
        }
    }
}
