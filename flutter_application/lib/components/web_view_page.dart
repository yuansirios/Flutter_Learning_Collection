import 'package:flutter/material.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:flutter_application/app/app_screen.dart';
import 'package:flutter_application/app/app_util.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  final String? title;
  final String url;
  final bool needBottomBar;
  final Function? didLoadCallBack;

  const WebViewPage(
      {Key? key,
      this.title = '',
      this.url = '',
      this.needBottomBar = false,
      this.didLoadCallBack})
      : super(key: key);

  @override
  State<WebViewPage> createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  late WebViewController _webViewController;
  bool loadingFail = false;
  String navTitle = '';

  final ValueNotifier<double> lineProgress = ValueNotifier<double>(0.0);

  @override
  void initState() {
    super.initState();
    navTitle = widget.title ?? '';
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppUtil.globalAppBar(
        title: Text(navTitle),
        bottom: PreferredSize(
          child: _progressBar(),
          preferredSize: const Size.fromHeight(1.0),
        ),
      ),
      body: loadingFail
          ? Column(
              children: [
                const Text('加载失败'),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        loadingFail = false;
                      });
                    },
                    child: const Text('点击重试'))
              ],
            )
          : Column(
              children: <Widget>[
                Expanded(
                  child: WebView(
                      initialUrl: widget.url,
                      javascriptMode: JavascriptMode.unrestricted,
                      navigationDelegate: (NavigationRequest request) {
                        return NavigationDecision.navigate;
                      },
                      javascriptChannels: _jsChannels(),
                      onWebViewCreated: _onWebViewCreated,
                      onProgress: _onProgress,
                      onPageFinished: _onPageFinished,
                      onWebResourceError: _onWebResourceError),
                ),
                Visibility(
                  visible: widget.needBottomBar,
                  child: Container(
                    width: AppScreen.width,
                    height: AppScreen.bottomSafeHeight + 40,
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                            onPressed: () {
                              _webViewController.goBack();
                            },
                            icon: const Icon(Icons.navigate_before_outlined)),
                        IconButton(
                            onPressed: () {
                              _webViewController.goForward();
                            },
                            icon: const Icon(Icons.navigate_next_outlined)),
                        IconButton(
                            onPressed: () {
                              _webViewController.reload();
                            },
                            icon: const Icon(Icons.refresh))
                      ],
                    ),
                  ),
                )
              ],
            ),
    );
  }

  _onWebViewCreated(controller) async {
    _webViewController = controller;
    _webViewController.clearCache();
    //本地h5页面
    // String filePath = 'assets/mock/h5.html';
    // String fileText = await rootBundle.loadString(filePath);
    // _webViewController.loadUrl(Uri.dataFromString(fileText,
    //         mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
    //     .toString());
  }

  _onProgress(int progress) {
    if (progress > 0) {
      lineProgress.value = progress / 100.0;
    }
  }

  _onPageFinished(url) {
    if (widget.didLoadCallBack != null) {
      widget.didLoadCallBack!();
    }

    //获取标题显示
    _webViewController
        .runJavascriptReturningResult("document.title")
        .then((result) {
      if (mounted) {
        setState(() {
          navTitle = result;
        });
      }
    });

    //JS注入传参，方法未实现会报异常
    // var header = {};
    // if (UserManager.isLogin) {
    //   header['token'] = UserManager.currentUser!.headerToken;
    // }
    // String code = App.languageCode;
    // if (code.isNotEmpty) {
    //   header['language'] = code;
    // }
    // String script = 'receiveUserInfo(${json.encode(header)})';
    // _webViewController.runJavascriptReturningResult(script);
  }

  _onWebResourceError(WebResourceError error) {
    if (mounted) {
      setState(() {
        loadingFail = true;
      });
    }
  }

  _jsChannels() {
    return <JavascriptChannel>{};
  }

  _progressBar() {
    return ValueListenableBuilder<double>(
        builder: (BuildContext context, double value, Widget? child) {
          return LinearProgressIndicator(
            backgroundColor: Colors.white.withOpacity(0),
            value: value == 1.0 ? 0 : value,
            valueColor: AlwaysStoppedAnimation<Color>(AppColor.themeColor),
          );
        },
        valueListenable: lineProgress);
  }
}
