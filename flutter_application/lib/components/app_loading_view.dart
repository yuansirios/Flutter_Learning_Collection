import 'package:flutter/material.dart';

import '../app/app_color.dart';

//加载中视图
class AppLoadingView extends StatelessWidget {
  final bool isDark;
  final Color? bgColor;
  const AppLoadingView({Key? key, this.isDark = false, this.bgColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color? color = bgColor;
    color ??= isDark ? AppColor.hexColor(0x23242A) : AppColor.bgColor;
    return Container(color: color, child: const Center(child: Text('加载中')));
  }
}
