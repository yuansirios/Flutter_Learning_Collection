import 'package:flutter/material.dart';

//封装 通用局部刷新工具类
//定义函数别名
typedef BuildWidget = Widget Function();

// ignore: must_be_immutable
class PartRefreshWidget extends StatefulWidget {
  const PartRefreshWidget(Key key, this._child) : super(key: key);
  final BuildWidget _child;

  @override
  State<StatefulWidget> createState() {
    // ignore: no_logic_in_create_state
    return PartRefreshWidgetState(_child);
  }
}

class PartRefreshWidgetState extends State<PartRefreshWidget> {
  BuildWidget child;

  PartRefreshWidgetState(this.child);

  @override
  Widget build(BuildContext context) {
    return child.call();
  }

  void update() {
    setState(() {});
  }
}
