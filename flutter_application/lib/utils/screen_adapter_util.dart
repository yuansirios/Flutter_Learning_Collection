//MARK:屏幕适配
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScreenAdapter {
  static setUp(BuildContext context) {
    //设计参考iPhone 6
    ScreenUtil.init(context,
        designSize: const Size(375, 667), minTextAdapt: true);
  }

  // 字体适配
  double setFontSize(num fontSize) => ScreenUtil().setSp(fontSize);
  // 适配高度
  double setHeight(num height) => ScreenUtil().setHeight(height);
  // 适配宽度
  double setWidth(num width) => ScreenUtil().setWidth(width);

  //获取状态栏高度
  getStatusBarHeight(){
    return ScreenUtil().statusBarHeight;
  }

  // // 屏幕宽度
  // double screenWidth = ScreenUtil().screenWidth;
  // // 屏幕高度
  // double screenHeight = ScreenUtil().screenHeight;
  // // statusBar高度
  // double statusBarHeight = ScreenUtil().statusBarHeight;
  // // bottomBar高度
  // double bottomBarHeight = ScreenUtil().bottomBarHeight;

  //打印设备信息
  void printScreenInformation() {
    debugPrint('Device width dp:${1.sw}dp | ${ScreenUtil().screenWidth}');
    debugPrint('Device height dp:${1.sh}dp | ${ScreenUtil().screenHeight}');

    debugPrint('Device pixel density:${ScreenUtil().pixelRatio}');
    debugPrint(
        'Bottom safe zone distance dp:${ScreenUtil().bottomBarHeight}dp');
    debugPrint('Status bar height dp:${ScreenUtil().statusBarHeight}dp');
    debugPrint(
        'The ratio of actual width to UI design:${ScreenUtil().scaleWidth}');
    debugPrint(
        'The ratio of actual height to UI design:${ScreenUtil().scaleHeight}');
    debugPrint('System font scaling:${ScreenUtil().textScaleFactor}');
    debugPrint('0.5 times the screen width:${0.5.sw}dp');
    debugPrint('0.5 times the screen height:${0.5.sh}dp');
    debugPrint('Screen orientation:${ScreenUtil().orientation}');

    debugPrint('width:80 ${setWidth(80)}');
    debugPrint('height:80 ${setHeight(80)}');
    debugPrint('fontSize:80 ${setFontSize(80)}');
  }
}
