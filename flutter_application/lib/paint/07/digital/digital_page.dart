import 'package:flutter/material.dart';
import 'package:flutter_application/paint/07/digital/digital_path.dart';
import 'package:flutter_application/paint/07/digital/digital_widget.dart';

/// 数字显示
class DigitalPage extends StatefulWidget {
  const DigitalPage({super.key});

  @override
  State<DigitalPage> createState() => _DigitalPageState();
}

class _DigitalPageState extends State<DigitalPage> {
  int _count = 0;

  final DigitalPath digitalPath = DigitalPath();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () {
            _count++;
            setState(() {});
          },
        ),
        body: Center(
            child: MultiDigitalWidget(
          colors: const [
            Colors.red,
            Colors.orange,
            Colors.yellow,
            Colors.green,
            Colors.blue,
            Colors.indigo,
            Colors.purple,
            Colors.black
          ],
          width: 76,
          spacing: 16,
          count: 8,
          value: _count,
        )),
      ),
    );
  }
}
