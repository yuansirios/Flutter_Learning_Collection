import 'package:flutter/material.dart';
import 'package:flutter_application/paint/07/boy/boy_painter.dart';

/// SVG绘制
class SvgPainterPage extends StatefulWidget {
  const SvgPainterPage({super.key});

  @override
  State<SvgPainterPage> createState() => _SvgPainterPageState();
}

class _SvgPainterPageState extends State<SvgPainterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: CustomPaint(
        size: const Size(300, 300),
        // painter: SnowPainter(),
        // painter: ShadowPainter(),
        painter: BoyPainter(),
      ),
    ));
  }
}
