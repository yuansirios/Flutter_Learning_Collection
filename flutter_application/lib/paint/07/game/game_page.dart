import 'package:flutter/material.dart';

import 'ctrl_painter.dart';
import 'outer_shell_painter.dart';

class GamePage extends StatefulWidget {
  const GamePage({super.key});

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
            width: 800 / 2,
            height: 1752 / 2,
            child: Stack(
              fit: StackFit.expand,
              children: [
                CustomPaint(
                  painter: OuterShellPainter(),
                ),
                CustomPaint(
                  painter: CtrlPainter(),
                )
              ],
            )),
      ),
    );
  }
}
