import 'package:flutter/material.dart';
import 'path_painter.dart';

/// 绘图动画
class Matirx4Page extends StatefulWidget {
  const Matirx4Page({super.key});

  @override
  State<Matirx4Page> createState() => _Matirx4PageState();
}

class _Matirx4PageState extends State<Matirx4Page>
    with SingleTickerProviderStateMixin {
  late AnimationController _ctrl;

  @override
  void initState() {
    super.initState();
    _ctrl = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: GestureDetector(
        onTap: _startAnimation,
        child: CustomPaint(
          size: const Size(400, 400),
          painter: PathPainter(animation: _ctrl),
        ),
      ),
    ));
  }

  void _startAnimation() {
    _ctrl.forward(from: 0);
  }
}
