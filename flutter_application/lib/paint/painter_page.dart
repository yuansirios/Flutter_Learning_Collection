import 'package:flutter/material.dart';
import 'package:flutter_application/paint/01/canvas_page.dart';
import 'package:flutter_application/paint/02/aeroplane.dart';
import 'package:flutter_application/paint/03/views/coo_page.dart';
import 'package:flutter_application/paint/04/animation_scanner.dart';
import 'package:flutter_application/paint/05/decorate_page.dart';
import 'package:flutter_application/paint/05/snow/snow_painter.dart';
import 'package:flutter_application/paint/06/matirx4_page.dart';
import 'package:flutter_application/paint/07/boy/svg_painter_page.dart';
import 'package:flutter_application/paint/07/digital/digital_page.dart';
import 'package:flutter_application/paint/07/game/game_page.dart';
import 'package:flutter_application/paint/07/wine/wine_bottle_page.dart';
import 'package:flutter_application/paint/08/save_canvas_page.dart';

class PainterPage extends StatefulWidget {
  const PainterPage({super.key});

  @override
  State<PainterPage> createState() => _PainterPageState();
}

class _PainterPageState extends State<PainterPage> {
  final List _dataList = [
    {'title': '画板', 'component': const CanvasPage()},
    {'title': '飞行棋', 'component': const AeroPlane()},
    {'title': '函数坐标图', 'component': const CooPage()},
    {'title': '绘制扫描动画', 'component': const AnimationScannerView()},
    {'title': '绘制动画', 'component': const Matirx4Page()},
    {'title': '阴影绘制', 'component': const DecoratePage()},
    {'title': 'SVG绘制', 'component': const SvgPainterPage()},
    {'title': '数字绘制', 'component': const DigitalPage()},
    {'title': '游戏机绘制', 'component': const GamePage()},
    {'title': '酒瓶绘制', 'component': const WineBottlePage()},
    {'title': '保存绘制', 'component': const SaveCanvasPage()},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('绘制'),
        ),
        body: ListView.separated(
          itemCount: _dataList.length,
          itemBuilder: (BuildContext context, int index) {
            var item = _dataList[index];
            return ListTile(
              title: Text(item['title']),
              onTap: () {
                var component = item["component"];
                if (component is Widget) {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => component));
                }
              },
            );
          },
          separatorBuilder: (BuildContext context, int index) {
            return const Divider(
              height: 1,
            );
          },
        ));
  }
}
