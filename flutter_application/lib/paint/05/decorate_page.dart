import 'package:flutter/material.dart';
import 'package:flutter_application/paint/05/box_decoration/shadow_painter1.dart';

/// 绘制阴影和雪花
class DecoratePage extends StatefulWidget {
  const DecoratePage({super.key});

  @override
  State<DecoratePage> createState() => _DecoratePageState();
}

class _DecoratePageState extends State<DecoratePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: CustomPaint(
        size: const Size(300, 300),
        // painter: SnowPainter(),
        // painter: ShadowPainter(),
        painter: ShadowPainter1(),
      ),
    ));
  }
}
