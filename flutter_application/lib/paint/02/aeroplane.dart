import 'package:flutter/material.dart';

import 'model/chess_value.dart';
import 'painter/board_painter.dart';
import 'painter/chess_painter.dart';

/// 飞行棋盘
class AeroPlane extends StatefulWidget {
  const AeroPlane({Key? key}) : super(key: key);

  @override
  State<AeroPlane> createState() => _AeroPlaneState();
}

class _AeroPlaneState extends State<AeroPlane> {
  ChessValue chessValue = ChessValue();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: AspectRatio(
          aspectRatio: 1,
          child: Stack(
            children: [
              ///将子树标记为一个单独的重绘区域，从而避免不必要的重绘，提高应用程序的性能。
              RepaintBoundary(
                child: CustomPaint(
                  size: MediaQuery.of(context).size,
                  painter: BoardPainter(),
                ),
              ),
              CustomPaint(
                size: MediaQuery.of(context).size,
                painter: ChessPainter(chessValue),
              )
            ],
          ),
        ),
      ),
    );
  }
}
