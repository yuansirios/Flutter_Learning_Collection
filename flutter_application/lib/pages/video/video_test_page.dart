import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_application/app/app.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:flutter_application/components/app_loading_view.dart';
import 'package:flutter_application/components/part_refresh_widget.dart';
import 'package:video_player/video_player.dart';

import '../../components/slider_custom_track_shape.dart';

class VideoTestPage extends StatefulWidget {
  const VideoTestPage({super.key});

  @override
  State<VideoTestPage> createState() => _VideoTestPageState();
}

class _VideoTestPageState extends State<VideoTestPage> {
  late VideoPlayerController _controller;
  bool _showCover = true;
  bool _isPlaying = false;
  Timer? _hideTimer;
  double _sliderValue = 0.0;
  String _videoUrl = "";

  GlobalKey<PartRefreshWidgetState> videoKey = GlobalKey();
  GlobalKey<PartRefreshWidgetState> progressKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _setUpController();
  }

  @override
  void dispose() {
    _hideTimer?.cancel();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Video'),
        ),
        body: PartRefreshWidget(videoKey, () {
          return _videoView();
        }));
  }

  _setUpController() {
    _videoUrl =
        'https://cdn.caomeikp.com/ebb5eac9bd0579568de28868e443253a/b23e6c45-ea19-4761-403b-3b3ddcd9867b.mp4';
    _controller = VideoPlayerController.networkUrl(Uri.parse(_videoUrl));
    _controller.addListener(_videoListener);
    _controller.initialize().then((_) {
      setState(() {});
    });
  }

  _videoListener() {
    _isPlaying = _controller.value.isPlaying;
    _sliderValue = _controller.value.position.inMilliseconds.toDouble();
    if (_sliderValue < 0) {
      _sliderValue = 0;
    }
    setState(() {});
  }

  /// 显示播放视图
  _showPlayCover(bool show) {
    _showCover = show;
    _refreshVideo();
  }

  /// 刷新视频视图
  _refreshVideo() {
    if (mounted) {
      App.refreshByKey(videoKey);
    }
  }

  /// 视频播放页面
  _videoView() {
    if (_controller.value.isInitialized) {
      return Stack(
        children: [
          GestureDetector(
              onTap: () {
                _showPlayCover(!_showCover);
              },
              child: VideoPlayer(_controller)),
          _progressView(),
          _playBtn()
        ],
      );
    } else {
      return const AppLoadingView();
    }
  }

  _playBtn() {
    return Center(
      child: AnimatedOpacity(
        opacity: _showCover ? 0.8 : 0.0,
        duration: const Duration(milliseconds: 400),
        child: ClipOval(
          child: Container(
              width: 80,
              height: 80,
              color: AppColor.blackLight4,
              child: IconButton(
                  onPressed: _togglePlayPause,
                  iconSize: 40,
                  icon: Icon(
                    _isPlaying ? Icons.pause : Icons.play_arrow,
                    color: AppColor.white,
                  ))),
        ),
      ),
    );
  }

  _progressView() {
    return PartRefreshWidget(progressKey, () {
      //做保护，防止异常
      var sliderMax = _controller.value.duration.inMilliseconds.toDouble();
      if (sliderMax < 0) {
        sliderMax = 0;
      }
      return Positioned(
        left: 0,
        right: 0,
        bottom: 0,
        child: AnimatedOpacity(
            opacity: _showCover ? 0.8 : 0.0,
            duration: const Duration(milliseconds: 400),
            child: Container(
                height: 40.0, // 进度条高度
                color: AppColor.blackLight4,
                child: Row(children: [
                  IconButton(
                      onPressed: _togglePlayPause,
                      icon: Icon(
                        _isPlaying ? Icons.pause : Icons.play_arrow,
                        color: AppColor.white,
                      )),
                  Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: _timeText(_controller.value.position,
                          textAlign: TextAlign.right)),
                  Expanded(
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        VideoProgressIndicator(
                          _controller,
                          allowScrubbing: true,
                          padding: EdgeInsets.zero,
                          colors: VideoProgressColors(
                            playedColor: AppColor.themeColor, // 已播放部分颜色
                            bufferedColor: AppColor.greyLight4, // 缓冲部分颜色
                            backgroundColor: AppColor.white, // 背景颜色
                          ),
                        ),
                        SliderTheme(
                          data: SliderThemeData(
                            trackHeight: 5,
                            thumbColor: AppColor.themeColor,
                            activeTrackColor: AppColor.transparent,
                            inactiveTrackColor: AppColor.transparent,
                            trackShape: SliderCustomTrackShape(addHeight: 0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Slider(
                              value: _sliderValue,
                              min: 0.0,
                              max: sliderMax,
                              onChanged: (double value) {
                                _stopPlay(needLog: false);
                                _sliderValue = value;
                                App.refreshByKey(progressKey);
                                // 计算新的播放位置并跳转
                                final Duration newPosition =
                                    Duration(milliseconds: value.toInt());
                                _controller.seekTo(newPosition);
                              },
                              onChangeEnd: (double value) {
                                // 拖动结束时跳转到新的播放位置
                                final Duration newPosition =
                                    Duration(milliseconds: value.toInt());
                                _controller.seekTo(newPosition);
                                _startPlay();
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: _timeText(_controller.value.duration)),
                ]))),
      );
    });
  }

  /// 播放或暂停
  _togglePlayPause() {
    if (_isPlaying) {
      _stopPlay();
    } else {
      _startPlay();
    }
  }

  /// 开始播放
  _startPlay() async {
    _isPlaying = true;
    _controller.play();
    _showPlayCover(true);
    _startHideTimer();
  }

  /// 暂停播放
  _stopPlay({bool needLog = true, bool startHiddenTime = true}) {
    _isPlaying = false;
    _controller.pause();
    _showPlayCover(true);
    if (startHiddenTime) {
      _startHideTimer();
    }
  }

  _startHideTimer() {
    _hideTimer?.cancel();
    _hideTimer = Timer(const Duration(seconds: 3), () {
      _showPlayCover(false);
    });
  }

  _timeText(Duration duration, {TextAlign textAlign = TextAlign.left}) {
    return SizedBox(
      width: 40,
      child: Text(
        _formatDuration(duration),
        textAlign: textAlign,
        style: TextStyle(
          color: AppColor.white,
          fontSize: 12.0,
        ),
      ),
    );
  }

  _formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }
}
