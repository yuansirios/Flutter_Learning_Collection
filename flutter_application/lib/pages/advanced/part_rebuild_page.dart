import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_application/app/app.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:flutter_application/components/part_refresh_widget.dart';
import 'package:get/state_manager.dart';

class PartRebuildPage extends StatefulWidget {
  const PartRebuildPage({super.key});

  @override
  State<PartRebuildPage> createState() => _PartRebuildPageState();
}

class _PartRebuildPageState extends State<PartRebuildPage> {
  //方式一：part_refresh_widget
  final GlobalKey<PartRefreshWidgetState> partKey = GlobalKey();
  //方式二：ValueListenableBuilder
  final ValueNotifier<bool> partNotif = ValueNotifier<bool>(false);
  //方式三：ChangeNotifier
  final RefreshNotifier refreshCtrl = RefreshNotifier();
  //方式四：StreamController
  final StreamController<bool> streamCtrl = StreamController();

  @override
  Widget build(BuildContext context) {
    App.log('widget build');
    return Scaffold(
        appBar: AppBar(
          title: const Text('局部刷新'),
        ),
        body: Center(
            child: Column(
          children: [
            _partRefresh(),
            const SizedBox(
              height: 20,
            ),
            _partRefresh2(),
            const SizedBox(
              height: 20,
            ),
            _partRefresh3(),
            const SizedBox(
              height: 20,
            ),
            _partRefresh4(),
            const SizedBox(
              height: 20,
            ),
            _partRefresh5(),
            const Padding(
              padding: EdgeInsets.all(20),
              child: Text('列举了小巧方便的方案，当然也可以用其他的状态管理实现，看你喜欢'),
            )
          ],
        )));
  }

  _partRefresh() {
    return PartRefreshWidget(partKey, () {
      App.log('part1 build');
      return Container(
        width: 200,
        height: 100,
        color: AppColor.randomColor(),
        alignment: AlignmentDirectional.center,
        child: InkWell(
          child: const Text('PartRefreshWidget刷新'),
          onTap: () {
            if (partKey.currentState != null) {
              partKey.currentState!.update();
            }
          },
        ),
      );
    });
  }

  _partRefresh2() {
    return ValueListenableBuilder<bool>(
        builder: (BuildContext context, bool value, Widget? child) {
          App.log('part2 build');
          return Container(
            width: 200,
            height: 100,
            alignment: AlignmentDirectional.center,
            color: value ? Colors.red : Colors.green,
            child: InkWell(
              child: const Text('ValueListenable刷新'),
              onTap: () {
                partNotif.value = !partNotif.value;
              },
            ),
          );
        },
        valueListenable: partNotif);
  }

  _partRefresh3() {
    return AnimatedBuilder(
        animation: refreshCtrl,
        builder: (context, widget) {
          App.log('part3 build');
          return Container(
            width: 200,
            height: 100,
            alignment: AlignmentDirectional.center,
            color: refreshCtrl.model.bgColor,
            child: InkWell(
              child: const Text('RefreshCtrl背景刷新'),
              onTap: () {
                refreshCtrl.refreshBg();
              },
            ),
          );
        });
  }

  _partRefresh4() {
    return StreamBuilder<bool>(
      stream: streamCtrl.stream,
      initialData: false, //初始显示的值，如果不设置 第一帧将会不显示
      builder: (context, snap) {
        App.log('part4 build');
        if (snap.hasData) {
          bool data = snap.data ?? false;
          return Container(
            width: 200,
            height: 100,
            alignment: AlignmentDirectional.center,
            color: data ? Colors.red : Colors.green,
            child: InkWell(
              child: const Text('StreamBuilder刷新'),
              onTap: () {
                streamCtrl.add(!data);
              },
            ),
          );
        }
        return Container();
      },
    );
  }

  _partRefresh5() {
    final color = Colors.red.obs;
    return Obx(() {
      App.log('part5 build');
      return Container(
        width: 200,
        height: 100,
        alignment: AlignmentDirectional.center,
        color: color.value,
        child: InkWell(
          child: const Text('GetX背景刷新'),
          onTap: () {
            color.value = AppColor.createMaterialColor(AppColor.randomColor());
          },
        ),
      );
    });
  }
}

class RefreshModel {
  String title = '';
  Color bgColor = Colors.red;
}

class RefreshNotifier extends ChangeNotifier {
  Color color = AppColor.red;
  RefreshModel model = RefreshModel();

  refreshBg() {
    model.bgColor = AppColor.randomColor();
    notifyListeners();
  }
}
