import 'package:hive/hive.dart';

@HiveType(typeId: 1)
class NovelChapterModel extends HiveObject {
  @HiveField(0, defaultValue: 0)
  int id = 0; // 文章ID
  @HiveField(1, defaultValue: 1)
  int idx = 1; // 章节号, 从1开始
  @HiveField(2, defaultValue: 0)
  int novelId = 0; //小说ID
  @HiveField(3, defaultValue: '')
  String title = ''; // 章节标题
  @HiveField(4, defaultValue: '')
  String content = ''; // 正文内容
  @HiveField(5, defaultValue: 0)
  int prevId = 0; // 前一章信息 (0 表示没有前一章, 即当前为和1章)
  @HiveField(6, defaultValue: 0)
  int nextId = 0; // 下一章ID

  int welth = 0; // 已弃用，请勿使用，章节价格(书币值)
  int contentUpdateTimeTs = 0; //内容更新时间
  int unlock = 1; // 1 已解锁 0 未解锁
  NovelChapterModel();

  @override
  String toString() {
    return '''
            id: $id,
            idx: $idx,
            novelId: $novelId,
            title: $title,
            prevId: $prevId,
            nextId: $nextId,
           ''';
  }

  //DB转对象
  NovelChapterModel.fromDBJson(Map<String, dynamic> data) {
    id = data['chapter_id'];
    idx = data['chapter_idx'];
    novelId = data['chapter_novel_id'];
    title = data['chapter_title'];
    content = data['chapter_content'];
    prevId = data['chapter_prev_id'];
    nextId = data['chapter_next_id'];
  }

  //存DB的json
  toDBJson() {
    return {
      'chapter_novel_id': novelId,
      'chapter_id': id,
      'chapter_idx': idx,
      'chapter_prev_id': prevId,
      'chapter_next_id': nextId,
      'chapter_title': title,
      'chapter_content': content
    };
  }
}
