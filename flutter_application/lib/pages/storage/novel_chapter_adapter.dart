import 'package:flutter_application/pages/storage/novel_chapter_model.dart';
import 'package:hive/hive.dart';

class NovelChapterAdapter extends TypeAdapter<NovelChapterModel> {
  @override
  NovelChapterModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };

    /**
  @HiveField(5, defaultValue: 0)
  int nextId = 0; // 下一章ID
  @HiveField(6, defaultValue: 0)
  int prevId = 0; // 前一章信息 (0 表示没有前一章, 即当前为和1章)

     */

    var chapter = NovelChapterModel();
    chapter.id = fields[0] as int;
    chapter.idx = fields[1] as int;
    chapter.novelId = fields[2] as int;
    chapter.title = fields[3] as String;
    chapter.content = fields[4] as String;
    chapter.prevId = fields[5] as int;
    chapter.nextId = fields[6] as int;
    return chapter;
  }

  @override
  int get typeId => 1;

  @override
  void write(BinaryWriter writer, NovelChapterModel obj) {
    writer
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.idx)
      ..writeByte(2)
      ..write(obj.novelId)
      ..writeByte(3)
      ..write(obj.title)
      ..writeByte(4)
      ..write(obj.content)
      ..writeByte(5)
      ..write(obj.prevId)
      ..writeByte(6)
      ..write(obj.nextId);
  }
}
