import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_application/app/app.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:flutter_application/pages/root/home_page.dart';

import 'library_page.dart';
import 'me_page.dart';

class RootPage extends StatefulWidget {
  const RootPage({super.key});

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {

    //显示状态栏，背景色透明
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      var channel = const MethodChannel('channel.method');
      await Future.delayed(const Duration(seconds: 5));
      channel.invokeMethod('appDidLoad');
    });
    
    super.initState();
  }

  int currentTabBarIndex = 1;

  bool showBanner = true;

  //MARK:各内容页
  final List<Widget> _pageList = [
    const LibraryPage(),
    const HomePage(),
    const MePage(),
  ];

  //MARK:底部栏
  List<BottomNavigationBarItem> _barItem() {
    return [
      const BottomNavigationBarItem(
          icon: Icon(Icons.access_alarm), label: 'Libaray'),
      const BottomNavigationBarItem(icon: Icon(Icons.ac_unit), label: 'Home'),
      const BottomNavigationBarItem(icon: Icon(Icons.settings), label: 'Me'),
    ];
  }

  @override
  Widget build(BuildContext context) {
    Widget navBar = BottomNavigationBar(
      onTap: (int index) {
        setState(() {
          currentTabBarIndex = index;
        });
      },
      currentIndex: currentTabBarIndex,
      items: _barItem(),
      unselectedItemColor: AppColor.grey,
      selectedItemColor: AppColor.themeColor,
      selectedFontSize: 12,
      type: BottomNavigationBarType.fixed,
    );

    return Scaffold(
        body: IndexedStack(
          index: currentTabBarIndex,
          children: _pageList,
        ),
        bottomNavigationBar: navBar);
  }
}
