import 'package:flutter/material.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:flutter_application/pages/advanced/async_load_page.dart';
import 'package:flutter_application/pages/advanced/part_rebuild_page.dart';
import 'package:flutter_application/pages/advanced/thread_model_page.dart';
import 'package:flutter_application/pages/animation/animated_controller_page.dart';
import 'package:flutter_application/pages/animation/animated_gradient_page.dart';
import 'package:flutter_application/pages/animation/animated_switch_page.dart';
import 'package:flutter_application/pages/animation/custom/animated_custom_page.dart';
import 'package:flutter_application/pages/combat/combat_contact_page.dart';
import 'package:flutter_application/pages/combat/combat_text_page.dart';
import 'package:flutter_application/pages/combat/expand_list/expand_list_page.dart';
import 'package:flutter_application/pages/storage/storage_page.dart';
import 'package:flutter_application/pages/ui/list_page.dart';
import 'package:flutter_application/pages/ui/toast_page.dart';
import 'package:flutter_application/pages/video/video_test_page.dart';
import 'package:flutter_application/paint/painter_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool showBanner = true;
  final ScrollController scrollController = ScrollController();

  final List _dataList = [
    {
      'section': 'UI',
      'list': [
        {'title': 'ListView【OK】', 'component': const ListPage()},
        {'title': 'ToastView【OK】', 'component': const ToastPage()},
      ]
    },
    {
      'section': 'Animation',
      'list': [
        {'title': '渐变效果【OK】', 'component': const AnimatedGradientPage()},
        {'title': '组件切换【OK】', 'component': const AnimatedSwitchPage()},
        {'title': '显示动画【OK】', 'component': const AnimatedControllerPage()},
        {'title': '自定义动画【OK】', 'component': const AnimatedCustom()},
      ]
    },
    {
      'section': '数据存储',
      'list': [
        {'title': '数据库【OK】', 'component': const StoragePage()},
        // {'title': 'HTTP编程', 'component': ''},
        // {'title': '本地存储', 'component': ''},
        // {'title': 'JSON解析', 'component': ''},
      ]
    },
    {
      'section': '进阶',
      'list': [
        {'title': '异步加载UI【OK】', 'component': const AsyncLoadPage()},
        {'title': '局部刷新有几种方式【OK】', 'component': const PartRebuildPage()},
        {'title': '线程模型（异步、并发、事件循环）【OK】', 'component': const ThreadModelPage()},
        {'title': '绘制', 'component': const PainterPage()},
        // {'title': '状态管理有哪几种', 'component': ''},
        // {'title': '跨组件通信有几种方式', 'component': ''},
        // {'title': '国际化怎么操作', 'component': ''},
        // {'title': '屏幕适配方案', 'component': ''},
      ]
    },
    {
      'section': '疑问',
      'list': [
        {'title': 'GetX为什么能刷新Stateless', 'component': ''},
        {'title': 'GetX为什么不需要context', 'component': ''},
      ]
    },
    {
      'section': '实战',
      'list': [
        {
          'title': '跳转Messenger&WhatsApp【OK】',
          'component': const CombatContactPage()
        },
        {'title': 'Text文本相关处理【OK】', 'component': const CombatTextPage()},
        {'title': '折叠列表', 'component': const ExpandListPage()},
        {'title': '视频调研', 'component': const VideoTestPage()},
      ]
    },
  ];

  @override
  void initState() {
    super.initState();
    addScrollViewLisenter();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('示例合集'),
        ),
        body: SafeArea(
          child: Stack(children: [
            ListView(
              controller: scrollController,
              children: _getListData(context),
            ),
            AnimatedPositioned(
                left: 0,
                right: 0,
                bottom: showBanner ? 0 : -60,
                duration: const Duration(milliseconds: 120),
                child: Container(
                  width: double.infinity,
                  height: 60,
                  color: AppColor.themeColor,
                )),
          ]),
        ));
  }

  addScrollViewLisenter() {
    double lastOffset = 0;
    scrollController.addListener(() {
      double currentPostion = scrollController.offset;
      if (currentPostion - lastOffset > 25) {
        lastOffset = currentPostion;
        //处理下拉刷新逻辑
        if (currentPostion < 0) {
          show();
        } else {
          hide();
        }
      } else if (lastOffset - currentPostion > 25) {
        var scrollExtent = scrollController.position.maxScrollExtent;
        lastOffset = currentPostion;
        if (currentPostion > scrollExtent) {
          hide();
        } else {
          show();
        }
      } else {
        if (currentPostion < 0) {
          show();
        }
      }
    });
  }

  show() {
    setState(() {
      showBanner = true;
    });
  }

  hide() {
    setState(() {
      showBanner = false;
    });
  }

  List<Widget> _getListData(BuildContext context) {
    var headerStyle = const TextStyle(fontSize: 20);
    var itemStyle = const TextStyle(fontSize: 16);

    List<Widget> list = [];

    for (int i = 0; i < _dataList.length; i++) {
      Widget header;
      var sectionDic = _dataList[i];
      var headerTitle = sectionDic['section'];
      if (headerTitle.length > 0) {
        header = ListTile(
          title: Text(
            "$headerTitle",
            style: headerStyle,
            textAlign: TextAlign.center,
          ),
        );
        list.add(header);
      }

      var itemList = sectionDic['list'];

      for (int j = 0; j < itemList.length; j++) {
        var item = itemList[j];
        var title = item["title"];
        list.add(Column(
          children: <Widget>[
            ListTile(
              title: Text(
                "【${j + 1}】$title",
                style: itemStyle,
              ),
              onTap: () {
                var component = item["component"];
                if (component is Widget) {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => component));
                }
              },
            ),
            const Divider(height: 1)
          ],
        ));
      }
    }
    return list;
  }
}
