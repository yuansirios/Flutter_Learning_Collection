import 'package:flutter/material.dart';

class AnimatedTweenPage extends StatefulWidget {
  const AnimatedTweenPage({Key? key}) : super(key: key);

  @override
  State<AnimatedTweenPage> createState() => _AnimatedTweenPageState();
}

class _AnimatedTweenPageState extends State<AnimatedTweenPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Color?> _animation;
  final Color _startColor = Colors.blue;
  final Color _endColor = Colors.red;

  Color _color = Colors.blue;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500))
      ..addListener(() {
        setState(() {
          _color = Color.lerp(_startColor, _endColor, _controller.value)!;
        });
      });

    _animation =
        ColorTween(begin: _startColor, end: _endColor).animate(_controller);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('颜色Tween'),
        ),
        body: Center(
            child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: Text("方式一"),
            ),
            GestureDetector(
              onTap: () {
                _controller.forward();
              },
              child: Container(
                height: 100,
                width: 100,
                color: _color,
                alignment: Alignment.center,
                child: const Text(
                  '点我变色',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(20.0),
              child: Text("方式二"),
            ),
            GestureDetector(
              onTap: () {
                _controller.forward();
              },
              child: Container(
                height: 100,
                width: 100,
                color: _animation.value,
                alignment: Alignment.center,
                child: const Text(
                  '点我变色',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
            ),
          ],
        )));
  }
}
