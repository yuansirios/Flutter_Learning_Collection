import 'package:flutter/material.dart';

class AnimatedListPage extends StatefulWidget {
  const AnimatedListPage({Key? key}) : super(key: key);

  @override
  State<AnimatedListPage> createState() => _AnimatedListPageState();
}

class _AnimatedListPageState extends State<AnimatedListPage>
    with SingleTickerProviderStateMixin {
  List<int> list = [];
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();

  void _addItem() {
    final int index = list.length;
    list.insert(index, index);
    _listKey.currentState?.insertItem(index);
  }

  void _removeItem() {
    final int index = list.length - 1;
    var item = list[index].toString();
    _listKey.currentState?.removeItem(
        index, (context, animation) => _buildItem(item, animation));
    list.removeAt(index);
  }

  Widget _buildItem(String item, Animation animation) {
    return SlideTransition(
      position: animation.drive(CurveTween(curve: Curves.easeIn)).drive(
          Tween<Offset>(begin: const Offset(1, 1), end: const Offset(0, 1))),
      child: Card(
        child: ListTile(
          title: Text(
            item,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('AnimatedList'),
      ),
      body: AnimatedList(
        key: _listKey,
        initialItemCount: list.length,
        itemBuilder: (BuildContext context, int index, Animation animation) {
          return _buildItem(list[index].toString(), animation);
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ElevatedButton(
            onPressed: () => _addItem(),
            child: const Icon(Icons.add),
          ),
          const SizedBox(
            width: 60,
          ),
          ElevatedButton(
            onPressed: () => _removeItem(),
            child: const Icon(Icons.remove),
          ),
        ],
      ),
    );
  }
}
