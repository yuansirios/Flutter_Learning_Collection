import 'package:flutter/material.dart';

class AnimatedCurvePage extends StatefulWidget {
  const AnimatedCurvePage({Key? key}) : super(key: key);

  @override
  State<AnimatedCurvePage> createState() => _AnimatedCurvePageState();
}

class _AnimatedCurvePageState extends State<AnimatedCurvePage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1000))
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          // _controller.forward();
        }
      });

    _animation = Tween(begin: 100.0, end: 200.0)
        .chain(CurveTween(curve: Curves.linear))
        .animate(_controller);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Curve'),
        ),
        body: Center(
          child: GestureDetector(
            onTap: () {
              _controller.forward();
            },
            child: Container(
              height: _animation.value,
              width: _animation.value,
              color: Colors.blue,
              alignment: Alignment.center,
              child: const Text(
                '点我变大',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ),
        ));
  }
}

class StairsCurve extends Curve {
  //阶梯的数量
  final int num;
  late double _perStairY;
  late double _perStairX;

  StairsCurve(this.num) {
    _perStairY = 1.0 / (num - 1);
    _perStairX = 1.0 / num;
  }

  @override
  double transformInternal(double t) {
    return _perStairY * (t / _perStairX).floor();
  }
}
