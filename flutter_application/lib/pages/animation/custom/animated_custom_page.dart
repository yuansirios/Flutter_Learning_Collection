import 'package:flutter/material.dart';
import 'package:flutter_application/pages/animation/custom/animated_builder_page.dart';
import 'package:flutter_application/pages/animation/custom/animated_curve_page.dart';
import 'package:flutter_application/pages/animation/custom/animated_list_page.dart';
import 'package:flutter_application/pages/animation/custom/animated_tween_builder_page.dart';
import 'package:flutter_application/pages/animation/custom/animated_tween_page.dart';

class AnimatedCustom extends StatefulWidget {
  const AnimatedCustom({Key? key}) : super(key: key);

  @override
  State<AnimatedCustom> createState() => _AnimatedCustomState();
}

class _AnimatedCustomState extends State<AnimatedCustom> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('自定义动画'),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            const AnimatedTweenBuilderPage()));
                  },
                  child: const Text("TweenAnimationBuilder")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AnimatedBuilderPage()));
                  },
                  child: const Text("AnimatedBuilder")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AnimatedListPage()));
                  },
                  child: const Text("AnimatedList")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AnimatedTweenPage()));
                  },
                  child: const Text("核心动画 - Tween")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const AnimatedCurvePage()));
                  },
                  child: const Text("核心动画 - Curve")),
            ]))));
  }
}
