/// 过渡动画
import 'package:flutter/material.dart';

class AnimatedGradientPage extends StatefulWidget {
  const AnimatedGradientPage({Key? key}) : super(key: key);

  @override
  State<AnimatedGradientPage> createState() => _AnimatedGradientPageState();
}

class _AnimatedGradientPageState extends State<AnimatedGradientPage> {
  bool _round = false;
  bool _position = false;
  bool _padding = false;
  bool _opacity = false;
  bool _textChange = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('渐变效果'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("1、AnimatedContainer"),
              ),
              _animatedContainer(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("2、AnimatedPositioned"),
              ),
              _animatedPosition(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("3、AnimatedPadding"),
              ),
              _animatedPadding(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("4、AnimatedOpacity"),
              ),
              _animatedOpacity(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("5、AnimatedTextStyle"),
              ),
              _animatedTextStyle(),
              const SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }

  _animatedContainer() {
    return Column(
      children: [
        AnimatedContainer(
          duration: const Duration(seconds: 1),
          width: _round ? 100 : 200,
          height: 200,
          decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(_round ? 100 : 0),
              boxShadow: _round
                  ? [const BoxShadow(spreadRadius: 8, blurRadius: 8)]
                  : null),
        ),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              _round = !_round;
            });
          },
          child: const Text("start"),
        ),
      ],
    );
  }

  //只能用在Stack中
  _animatedPosition() {
    return SizedBox(
      width: 400,
      height: 300,
      child: Stack(
        children: [
          AnimatedPositioned(
              duration: const Duration(seconds: 1),
              top: _position ? 0 : 100,
              bottom: _position ? 100 : 50,
              left: _position ? 100 : 50,
              right: _position ? 40 : 100,
              child: Container(
                color: Colors.grey,
              )),
          Align(
              alignment: const Alignment(0, 0.8),
              child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      _position = !_position;
                    });
                  },
                  child: const Text("start")))
        ],
      ),
    );
  }

  //padding改变的动画
  _animatedPadding() {
    return Column(
      children: [
        AnimatedPadding(
          padding: EdgeInsets.only(left: _padding ? 0.0 : 100.0),
          duration: const Duration(milliseconds: 200),
          child: const FlutterLogo(),
        ),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                _padding = !_padding;
              });
            },
            child: const Text("start"))
      ],
    );
  }

  //透明动画
  _animatedOpacity() {
    return Column(
      children: [
        AnimatedOpacity(
          duration: const Duration(seconds: 1),
          opacity: _opacity ? 1.0 : 0,
          child: const FlutterLogo(),
        ),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                _opacity = !_opacity;
              });
            },
            child: const Text("start"))
      ],
    );
  }

  //透明动画
  _animatedTextStyle() {
    return Column(
      children: [
        AnimatedDefaultTextStyle(
            style: TextStyle(
                fontSize: _textChange ? 28 : 20,
                color: _textChange ? Colors.red : Colors.green,
                fontWeight: _textChange ? FontWeight.bold : null,
                letterSpacing: _textChange ? 8.0 : 0),
            duration: const Duration(seconds: 1),
            child: const Text("just to test")),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                _textChange = !_textChange;
              });
            },
            child: const Text("start"))
      ],
    );
  }
}
