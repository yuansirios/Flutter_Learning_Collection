import 'package:flutter/material.dart';

// 显示动画

class AnimatedControllerPage extends StatefulWidget {
  const AnimatedControllerPage({Key? key}) : super(key: key);

  @override
  State<AnimatedControllerPage> createState() => _AnimatedControllerPageState();
}

class _AnimatedControllerPageState extends State<AnimatedControllerPage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  @override
  void initState() {
    _controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('显示动画'),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("1、旋转"),
              ),
              _rotationView(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("2、淡入淡出"),
              ),
              _fadeView(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("3、尺寸剪裁"),
              ),
              _sizeView(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("4、平移动画"),
              ),
              _slideView(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("5、Positioned"),
              ),
              _posittioned(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("6、装饰动画"),
              ),
              _decorated(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("7、Icon动画"),
              ),
              _iconView(),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    _controller.repeat(reverse: true);
                  },
                  child: const Text("start")),
              ElevatedButton(
                  onPressed: () {
                    _controller.stop();
                  },
                  child: const Text("stop"))
            ]))));
  }

  _rotationView() {
    return RotationTransition(
      turns: _controller,
      child: const FlutterLogo(
        size: 80,
      ),
    );
  }

  _fadeView() {
    return FadeTransition(
      opacity: _controller,
      child: const FlutterLogo(
        size: 80,
      ),
    );
  }

  _sizeView() {
    return SizeTransition(
      sizeFactor: _controller,
      axis: Axis.horizontal, //裁剪方向
      child: const FlutterLogo(
        size: 80,
      ),
    );
  }

  _slideView() {
    return SlideTransition(
      position: Tween(begin: const Offset(0, 0), end: const Offset(1, 0))
          .animate(_controller),
      child: const FlutterLogo(
        size: 80,
      ),
    );
  }

  // AnimatedPositioned显示版本
  _posittioned() {
    return Container(
      width: 300,
      height: 300,
      color: Colors.grey,
      child: Stack(
        children: [
          PositionedTransition(
            rect: RelativeRectTween(
                    begin: const RelativeRect.fromLTRB(0, 0, 200, 200),
                    end: const RelativeRect.fromLTRB(100, 100, 0, 0))
                .animate(_controller),
            child: const FlutterLogo(
              size: 80,
            ),
          )
        ],
      ),
    );
  }

  _decorated() {
    return DecoratedBoxTransition(
      decoration: DecorationTween(
              begin: BoxDecoration(
                  color: Colors.grey, borderRadius: BorderRadius.circular(50)),
              end: const BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.zero))
          .animate(_controller),
      child: const FlutterLogo(
        size: 80,
      ),
    );
  }

  _iconView() {
    return AnimatedIcon(icon: AnimatedIcons.arrow_menu, progress: _controller);
  }
}
