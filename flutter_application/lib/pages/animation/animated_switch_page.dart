//组件切换
import 'package:flutter/material.dart';

class AnimatedSwitchPage extends StatefulWidget {
  const AnimatedSwitchPage({Key? key}) : super(key: key);

  @override
  State<AnimatedSwitchPage> createState() => _AnimatedSwitchPageState();
}

class _AnimatedSwitchPageState extends State<AnimatedSwitchPage> {
  bool _switcher = false;
  bool _crossFade = false;
  int _count = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('组件切换'),
        ),
        body: SingleChildScrollView(
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("1、AnimatedSwitcher"),
              ),
              _animatedSwitcher(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("2、Switcher Demo"),
              ),
              _switcherDemo(),
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text("3、AnimatedCrossFade"),
              ),
              _animatedCrossFade(),
            ]))));
  }

  //只是简单实用stack容器讲新旧元素全部叠加
  _animatedSwitcher() {
    return Column(
      children: [
        AnimatedSwitcher(
          duration: const Duration(seconds: 1),
          child: _switcher
              ? const Text('Hi')
              : const FlutterLogo(
                  size: 48,
                ),
        ),
        const SizedBox(
          height: 20,
        ),
        AnimatedSwitcher(
          duration: const Duration(seconds: 1),
          reverseDuration: const Duration(seconds: 1),
          child: _switcher
              ? const Text('旋转')
              : const FlutterLogo(
                  size: 48,
                ),
          transitionBuilder: (child, animation) {
            return RotationTransition(
              turns: animation,
              child: child,
            );
          },
        ),
        const SizedBox(
          height: 20,
        ),
        AnimatedSwitcher(
          duration: const Duration(seconds: 1),
          reverseDuration: const Duration(seconds: 1),
          child: _switcher
              ? const Text('淡入淡出')
              : const FlutterLogo(
                  size: 48,
                ),
          transitionBuilder: (child, animation) {
            return FadeTransition(
              opacity: animation,
              child: child,
            );
          },
        ),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                _switcher = !_switcher;
              });
            },
            child: const Text("start"))
      ],
    );
  }

  //switcher demo
  _switcherDemo() {
    return Column(
      children: [
        AnimatedSwitcher(
            duration: const Duration(seconds: 3),
            transitionBuilder: ((child, animation) {
              return RotationTransition(turns: animation, child: child);
            }),
            layoutBuilder: (currentChild, previousChildren) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  if (currentChild != null) currentChild,
                  ...previousChildren,
                ],
              );
            },
            child: Text(
              "$_count",
              key: ValueKey(_count),
            )),
        const SizedBox(
          height: 32,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                _count++;
              });
            },
            child: const Text("start"))
      ],
    );
  }

  //切换组件尺寸不同时，比Switcher过渡更平滑
  _animatedCrossFade() {
    return Column(
      children: [
        AnimatedCrossFade(
            firstChild: const Text("第1个组件"),
            secondChild: const Text("第2个组件"),
            crossFadeState: _crossFade
                ? CrossFadeState.showFirst
                : CrossFadeState.showSecond,
            duration: const Duration(seconds: 1)),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {
                _crossFade = !_crossFade;
              });
            },
            child: const Text("start"))
      ],
    );
  }
}
