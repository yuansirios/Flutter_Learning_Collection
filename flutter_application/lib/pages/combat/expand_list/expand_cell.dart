import 'package:flutter/material.dart';

class ExpandCell extends StatelessWidget {
  const ExpandCell({
    Key? key,
    required this.title,
    this.icon = const Icon(
      Icons.expand_more,
      size: 24,
    ),
    this.expand = true,
    required this.body,
    required this.onChange,
    this.showBorder = true,
    this.padding = const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
  }) : super(key: key);

  final Widget title;

  final bool expand;

  final Widget body;

  final Function(bool) onChange;

  final Widget icon;

  final EdgeInsets padding;

  final bool showBorder;

  static const Color borderColor = Color.fromRGBO(235, 237, 240, 1);
  static const Duration sleep = Duration(milliseconds: 120);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      key: key,
      child: Column(
        children: <Widget>[
          InkWell(
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 0.5,
                          color:
                              showBorder ? borderColor : Colors.transparent))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: title,
                  ),
                  AnimatedRotation(
                    duration: sleep,
                    turns: expand ? 0.5 : 1,
                    child: icon,
                  )
                ],
              ),
            ),
            onTap: () => onChange(!expand),
          ),
          ExpandBody(
            sleep: sleep,
            value: expand,
            child: Container(padding: padding, child: body),
          )
        ],
      ),
    );
  }
}

class ExpandBody extends StatefulWidget {
  const ExpandBody({
    required this.child,
    required this.value,
    required this.sleep,
    Key? key,
  }) : super(key: key);

  final Widget child;
  final bool value;
  final Duration sleep;

  @override
  ExpandBodyState createState() => ExpandBodyState();
}

class ExpandBodyState extends State<ExpandBody> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return AnimatedSize(
        // ignore: deprecated_member_use
        vsync: this,
        duration: widget.sleep,
        child: Container(
          constraints: BoxConstraints(
            maxHeight: widget.value ? double.infinity : 0,
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                child: widget.child,
              )
            ],
          ),
        ));
  }
}
