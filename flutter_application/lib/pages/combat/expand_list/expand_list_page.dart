import 'package:flutter/material.dart';
import 'package:flutter_application/pages/combat/expand_list/expand_cell.dart';

class ExpandListPage extends StatefulWidget {
  const ExpandListPage({super.key});

  @override
  State<ExpandListPage> createState() => _ExpandListPageState();
}

class _ExpandListPageState extends State<ExpandListPage> {
  bool expand = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('折叠列表'),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            ExpandCell(
              title: const Text('点我展开'),
              padding: EdgeInsets.zero,
              body: Container(
                color: Colors.amber,
                height: 40,
                child: const Text('展开的内容'),
              ),
              expand: expand,
              onChange: (bool value) {
                setState(() {
                  expand = !expand;
                });
              },
            )
          ]),
        ));
  }
}
