import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application/app/app.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:flutter_application/app/app_style.dart';
import 'package:flutter_application/app/app_util.dart';
import 'package:flutter_application/components/text_position_widget.dart';

class CombatTextPage extends StatefulWidget {
  const CombatTextPage({super.key});

  @override
  State<CombatTextPage> createState() => _CombatTextPageState();
}

class _CombatTextPageState extends State<CombatTextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Text相关处理'),
        ),
        body: Center(
            child: Column(
          children: [_hightText(), _linkText(), _largeLengthText()],
        )));
  }

  _hightText() {
    TextStyle defaultStyle =
        AppStyle.textNormal(fontSize: 16, color: AppColor.textColor);
    TextStyle lightStyle =
        AppStyle.textNormal(fontSize: 16, color: AppColor.themeColor);

    var content = 'aasdfafAdafABBbaB';
    var hight = 'a';
    Widget titleView =
        AppUtil.hightTextView(content, hight, defaultStyle, lightStyle);

    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.all(20.0),
          child: Text('1、文字高亮'),
        ),
        titleView
      ],
    );
  }

  _linkText() {
    String lightText = 'tel:888888';
    String lightText2 = 'webUrl:9999';
    String text = "aljkfalskfjlsadjfl $lightText adsjkjalsdf $lightText2";

    // 默认普通文本的样式
    TextStyle defTextStyle =
        AppStyle.textNormal(fontSize: 13, color: Colors.grey);
    // 默认高亮文本的样式
    TextStyle defLightStyle =
        AppStyle.textNormal(fontSize: 13, color: AppColor.themeColor);

    List<TextSpan> spans = [];
    int start = 0;
    int end;

    while ((end = text.indexOf(lightText, start)) != -1) {
      spans
          .add(TextSpan(text: text.substring(start, end), style: defTextStyle));
      spans.add(TextSpan(
        text: lightText,
        style: defLightStyle,
        recognizer: TapGestureRecognizer()
          ..onTap = () => {App.showToast('点击了tel')},
      ));
      start = end + lightText.length;
    }

    while ((end = text.indexOf(lightText2, start)) != -1) {
      spans.add(TextSpan(
        text: text.substring(start, end),
        style: defTextStyle,
      ));
      spans.add(TextSpan(
          text: lightText2,
          style: defLightStyle,
          recognizer: TapGestureRecognizer()
            ..onTap = () => {App.showToast('点击了Url')}));
      // 设置下一段要截取的开始位置
      start = end + lightText2.length;
    }
    spans.add(
      TextSpan(text: text.substring(start, text.length), style: defTextStyle),
    );

    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.all(20.0),
          child: Text('2、文字可点击'),
        ),
        RichText(
          text: TextSpan(children: spans),
        )
      ],
    );
  }

  _largeLengthText() {
    return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            const Text('3、文字超长处理'),
            const SizedBox(
              height: 20,
            ),
            TextPositionWidget('中间省略号' * 20,
                style: AppStyle.textNormal(fontSize: 16)),
            TextPositionWidget(
              '尾部省略号' * 20,
              style: AppStyle.textNormal(fontSize: 16),
              wxOverflow: WXTextOverflow.ellipsisEnd,
            ),
            TextPositionWidget(
              '显示不下隐藏' * 20,
              style: AppStyle.textNormal(fontSize: 16),
              wxOverflow: WXTextOverflow.fade,
            ),
          ],
        ));
  }
}
