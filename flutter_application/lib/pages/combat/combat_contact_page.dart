import 'package:flutter/material.dart';
import 'package:flutter_application/app/app.dart';
import 'package:url_launcher/url_launcher.dart';

class CombatContactPage extends StatefulWidget {
  const CombatContactPage({Key? key}) : super(key: key);

  @override
  State<CombatContactPage> createState() => _CombatContactPageState();
}

class _CombatContactPageState extends State<CombatContactPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('联系我们'),
        ),
        body: Center(
            child: Column(
          children: [
            ElevatedButton(
                onPressed: _toMessenger, child: const Text('Messenger')),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: _toWhatsApp, child: const Text('WhatsApp'))
          ],
        )));
  }

  _toMessenger() async {
    var value = '100086948806191';
    value = '100301902888161';
    
    String path = 'fb-messenger://user/$value';

    if (App.isIOS) {
      path = 'https://m.me/$value';
    }
    final Uri url = Uri.parse(path);
    if (!await launchUrl(url)) {
      App.showError();
    }
  }

  _toWhatsApp() async {
    var value = '+8613675841069';
    var path = "whatsapp://send?phone=$value";
    Uri url = Uri.parse(path);
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      path = 'https://api.whatsapp.com/send?phone=$value"';
      url = Uri.parse(path);
      if (!await launchUrl(url)) {
        App.showError();
      }
    }
  }
}
