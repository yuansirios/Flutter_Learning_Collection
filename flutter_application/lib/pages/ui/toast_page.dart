import 'package:flutter/material.dart';
import 'package:flutter_application/components/web_view_page.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class ToastPage extends StatefulWidget {
  const ToastPage({Key? key}) : super(key: key);
  @override
  State<ToastPage> createState() => _ToastPageState();
}

class _ToastPageState extends State<ToastPage> {
  @override
  void initState() {
    super.initState();

    EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.dark
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = false
    ..dismissOnTap = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Toast示例'),
        ),
        body: Center(
          child: Column(
            children: [
              ElevatedButton(
                  onPressed: () async {
                    EasyLoading.showToast('showToast');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showSuccess('showSuccess');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showError('showError');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showInfo('showInfo');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.show(status:'loading');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showProgress(0.1,status: 'showProgress');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showProgress(0.3,status: 'showProgress');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showProgress(0.8,status: 'showProgress');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.showProgress(1,status: 'showProgress');
                    await Future.delayed(const Duration(seconds: 1));
                    EasyLoading.dismiss();
                  },
                  child: const Text('点我')),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const WebViewPage(
                              url:
                                  'https://pub.dev/packages/flutter_easyloading/example',
                            )));
                  },
                  child: const Text('官网示例'))
            ],
          ),
        ));
  }
}
