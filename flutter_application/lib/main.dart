import 'package:flutter/material.dart';
import 'package:flutter_application/pages/root/root.dart';
import 'package:flutter_application/utils/screen_adapter_util.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

Future<void> main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    ScreenAdapter.setUp(context);
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const RootPage(),
      builder: EasyLoading.init(),
    );
  }
}
