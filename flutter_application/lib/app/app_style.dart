import 'package:flutter/material.dart';
import 'package:flutter_application/app/app_color.dart';

class AppStyle {
  //封面阴影
  static List<BoxShadow> get coverShadow {
    return const [BoxShadow(color: Color(0x22000000), blurRadius: 8)];
  }

  //全局阴影
  static List<BoxShadow> get appShadow {
    return [
      BoxShadow(
        blurRadius: 3, //阴影范围
        spreadRadius: 1, //阴影浓度
        color: Colors.black.withOpacity(0.1), //阴影颜色
      ),
    ];
  }

  //渐变色
  static LinearGradient get appLinear {
    return LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      stops: const [0, 0.4, 1],
      colors: [
        AppColor.hexColor(0xF4813B),
        AppColor.hexColor(0xF4813B),
        AppColor.hexColor(0xF43D3D),
      ],
    );
  }

  ///****************** 字体设置 ******************/

  static TextStyle titleStyle() => AppStyle.textBold(fontSize: 20);

  static TextStyle textNormal(
      {double? fontSize,
      Color? color,
      double? height,
      String? fontFamily,
      TextOverflow? overflow}) {
    color = color ?? AppColor.textColor;
    return TextStyle(
        fontSize: fontSize,
        color: color,
        height: height,
        fontFamily: fontFamily,
        overflow: overflow);
  }

  static TextStyle textBold(
      {double? fontSize,
      Color? color,
      double? height,
      String? fontFamily,
      TextOverflow? overflow}) {
    color = color ?? AppColor.textColor;
    return TextStyle(
        fontSize: fontSize,
        color: color,
        height: height,
        fontWeight: FontWeight.bold,
        fontFamily: fontFamily,
        overflow: overflow);
  }

  static TextStyle get f12 {
    return textNormal(fontSize: 12, overflow: TextOverflow.ellipsis);
  }

  static TextStyle get f16 {
    return textNormal(fontSize: 16, overflow: TextOverflow.ellipsis);
  }

  static TextStyle get f16Blod {
    return textBold(fontSize: 16, overflow: TextOverflow.ellipsis);
  }
}
