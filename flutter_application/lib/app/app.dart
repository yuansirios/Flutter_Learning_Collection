import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application/components/part_refresh_widget.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class App {
  static void log(message) {
    debugPrint('[APP LOG] $message');
  }

  ///************** 资源路径 **************/
  static String assetsPath(String name) {
    return "assets/$name";
  }

  static String imagePath(String name) {
    return "assets/images/$name";
  }

  //判断机型
  static bool get isIOS {
    return Platform.isIOS;
  }

  ///************** Toast相关 **************/
  static void showLoading({String? title}) {
    EasyLoading.show(status: title, maskType: EasyLoadingMaskType.black);
  }

  static void hideLoading() {
    EasyLoading.dismiss();
  }

  static void showToast(msg) {
    hideLoading();
    EasyLoading.showToast(msg);
  }

  static void showSuccess(msg) {
    hideLoading();
    EasyLoading.showSuccess(msg);
  }

  static void showError({String? msg}) {
    hideLoading();
    msg ??= '操作失败';
    EasyLoading.showToast(msg);
  }

  /// 局部刷新
  static refreshByKey(GlobalKey<PartRefreshWidgetState> key) {
    if (key.currentState != null) {
      key.currentState!.update();
    }
  }
}
