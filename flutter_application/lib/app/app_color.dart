import 'dart:math';
import 'package:flutter/material.dart';

class AppColor {
  //透明色
  static Color transparent = Colors.transparent;

  //白色
  static Color white = Colors.white;

  //黑色
  static Color black = Colors.black;
  static Color blackLight4 = black.withOpacity(0.4);
  static Color blackLight7 = black.withOpacity(0.7);

  //黑色
  static Color grey = Colors.grey;
  static Color greyLight2 = grey.withOpacity(0.2);
  static Color greyLight4 = grey.withOpacity(0.4);

  //红色
  static Color red = AppColor.hexColor(0xFE5A53);

  //背景色
  static Color bgColor = const Color(0xEFEFEFEF);

  //默认文字颜色
  static Color textColor = hexColor(0x3D3D3D);
  
  //主题颜色
  static Color themeColor = hexColor(0xF4813B);

  //骨架图颜色
  static Color skColor = AppColor.hexColor(0xF2F2F2);

  //随机颜色
  static Color randomColor() {
    return Color.fromRGBO(
        Random().nextInt(256), Random().nextInt(256), Random().nextInt(256), 1);
  }

  /// 十六进制颜色，
  /// hex, 十六进制值，例如：0xffffff,
  /// alpha, 透明度 [0.0,1.0]
  static Color hexColor(int hex, {double alpha = 1}) {
    if (alpha < 0) {
      alpha = 0;
    } else if (alpha > 1) {
      alpha = 1;
    }
    return Color.fromRGBO((hex & 0xFF0000) >> 16, (hex & 0x00FF00) >> 8,
        (hex & 0x0000FF) >> 0, alpha);
  }

  ///创建Material风格的color
  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map swatch = <int, Color>{};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    for (var strength in strengths) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    }
    return MaterialColor(color.value, swatch as Map<int, Color>);
  }
}
