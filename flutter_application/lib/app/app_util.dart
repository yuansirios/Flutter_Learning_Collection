import 'package:flutter/material.dart';
import 'package:flutter_application/app/app_color.dart';
import 'package:substring_highlight/substring_highlight.dart';

class AppUtil {
  // 边框颜色
  static BoxDecoration borderDecoration({Color? color}) {
    return BoxDecoration(
        border: Border.all(color: color ?? AppColor.randomColor(), width: 0.5));
  }

  //全局导航栏
  static PreferredSizeWidget? globalAppBar(
      {bool? centerTitle,
      Widget? title,
      List<Widget>? actions,
      Widget? leading,
      double? leadingWidth,
      PreferredSizeWidget? bottom}) {
    return AppBar(
        centerTitle: centerTitle,
        elevation: 0.2,
        title: title,
        leading: leading,
        leadingWidth: leadingWidth,
        bottom: bottom,
        actions: actions);
  }


  //文本高亮视图
  static Widget hightTextView(String text, String lightText,
      TextStyle defaultStyle, TextStyle lightStyle,
      {int? maxLines, TextAlign textAlign = TextAlign.left}) {
    return SubstringHighlight(
      text: text,
      term: lightText,
      maxLines: maxLines,
      textAlign: textAlign,
      textStyle: defaultStyle,
      textStyleHighlight: lightStyle,
    );
  }

}
