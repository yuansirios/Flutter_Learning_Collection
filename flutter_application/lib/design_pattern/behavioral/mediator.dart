// 把聊天室作为中介 Mediator
abstract class ChatRoomMediator {
  String showMessage(User user, String message);
}

class ChatRoom implements ChatRoomMediator {
  @override
  String showMessage(User user, String message) {
    String dateTime = DateTime.now().toLocal().toString();
    String sender = user._name;
    return "$dateTime [ $sender ]: $message";
  }
}

// 用户发消息时需经过 Mediator 的调用
// 即将不同的 user 实体等操作抽象到 Mediator 中去，在 Mediator 做聚合一些处理
class User {
  final String _name;
  final ChatRoomMediator _mediator;

  User(this._name, this._mediator);

  String send(String message) => _mediator.showMessage(this, message);
}

/*
中介者模式

目的
新增一个中介者 (mediator) 来控制两个实体的交流，进而减少两者的耦合。

何时使用
当有两个实体要沟通时，沟通过程十分复杂，需要有效管理时。
 */
void main(List<String> args) {
  ChatRoomMediator mediator = ChatRoom();
  User jay = User("Jay", mediator);
  User wei = User("Wei", mediator);

  print("""
    ${jay.send("Hello !")}
    ${wei.send("Hi !")}
    """);

  // Mediator👇👇👇
  // 2020-11-25 09:17:19.070966 [ Jay ]: Hello !
  // 2020-11-25 09:17:19.073280 [ Wei ]: Hi !
}
