// 创建一个 Memento 用来记忆输入内容
class EditorMemento {
  final String _content;

  EditorMemento(this._content);
}

// editor 可以使用 memento 存档或复原
class Editor {
  String _content = "";

  void type(String content) => _content += '\n$content';

  EditorMemento save() => EditorMemento(_content);

  String restore(EditorMemento memento) => _content = memento._content;
}

/*
记事本模式

目的
在不影响封装的情况下，让实体能够 Restore 到某一个状态。

何时使用
当你需要暂时将实体存下 Snapshot 以便在未来方便复原。
 */
void main(List<String> args) {
  Editor editor = Editor();

  // 打两行字然后存档
  editor.type("First Line.");
  editor.type("Second Line.");
  var saved = editor.save();

  // 现在不小心碰到键盘了，想要复原
  editor.type("AAAAAAAAAAAAAAAAAAAAAAAAA");
  editor.restore(saved);

  print(editor._content);

  // Memento👇👇👇

  // First Line.
  // Second Line.
}
