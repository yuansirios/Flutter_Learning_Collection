class Song {
  String name;

  Song(this.name);
}

/*
迭代器模式

目的
在不暴露内部逻辑情况下，按顺序将集合实体显示出来。

何时使用
想要统一 Traversal 方法，或想支援多层 Traversal 时。
 */
void main(List<String> args) {
  List<Song> mp3Player = [];

  mp3Player.add(Song("愛をこめて花束を"));
  mp3Player.add(Song("輝く月のように"));
  mp3Player.add(Song("やさしい気持ちで"));

  // Dart 可以直接利用現成 Iterator.
  final iterator = mp3Player.iterator;
  var result = "";

  // 类似遍历单向链表
  while (iterator.moveNext()) {
    result += "Playing ... ${iterator.current.name}.\n";
  }

  print(result);

  // Iterator👇👇👇
  // Playing ... 愛をこめて花束を.
  // Playing ... 輝く月のように.
  // Playing ... やさしい気持ちで.
}
