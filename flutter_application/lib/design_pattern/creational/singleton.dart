class SingletonObject {
  //单例，使用 static 使其可在factory中调用
  static final SingletonObject _singleton = SingletonObject._();

  //工厂方法，生产单例
  factory SingletonObject() => _singleton;
  //or factory SingletonObject.getSingleton() => _singleton;

  //单例构造方法，需私有
  SingletonObject._() {
    //do something
  }
}

/*
单例模式
目的
确保建立出来的实体只会有一个实例，并且提供给 client 一个全局变量来使用他。

何时使用
当你想确保实体不会被重复产生时。
 */
void main(List<String> args) {
  var singleton1 = SingletonObject();
  var singleton2 = SingletonObject();

  print("""
    ${singleton1 == singleton2}
    ${identical(singleton1, singleton2)}
    """);

  // Singleton👇👇👇
  // true
  // true
}
