// 抽象类
abstract class Door {
  double width;
  double height;

  Door(this.width, this.height);
}

// 子类实现抽象类
class WoodenDoor implements Door {
  @override
  double height;
  @override
  double width;

  WoodenDoor(this.width, this.height);
}

// 建立一个工厂创建实例
class DoorFactory {
  //提供实例生产方法
  static Door createDoor(double width, double height) {
    //隐藏实例生产逻辑
    return WoodenDoor(width, height);
  }
}

/*
01-简单工厂
目的
给 Client 产生东西的方法，但不暴露产生的逻辑。

何时使用
要产生的实体需要比较多一些逻辑时，避免在产生多个实体造成太多 Code 重复。
 */
void main(List<String> args) {
  var door1 = DoorFactory.createDoor(100, 200);
  var door2 = DoorFactory.createDoor(50, 100);
  print("""
    door1's width = 
    ${door1.width}
    
    door2's height = 
    ${door2.height}
    """);

  // SimpleFactory👇👇👇
  // door1's width =
  // 100.0

  // door2's height =
  // 100.0
}
