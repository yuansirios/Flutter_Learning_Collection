// 抽象类
abstract class Interviewer {
  String askQuestions();
}

// 实现askQuestions行为的子类1
class CommunityExecutive extends Interviewer {
  @override
  String askQuestions() {
    return 'What is Community Building ?';
  }
}

// 实现askQuestions行为的子类2
class Developer extends Interviewer {
  @override
  String askQuestions() {
    return 'What is Design Pattern ?';
  }
}

// 工厂manager，将Interviewer实例创建方法交给子类实现
abstract class HiringManager {
  Interviewer _makeInterviewer();

  String takeInterview() {
    var interviewer = _makeInterviewer();
    return interviewer.askQuestions();
  }
}

// 生产Developer的manager
class DevelopmentManager extends HiringManager {
  @override
  Interviewer _makeInterviewer() {
    return Developer();
  }
}

// 生产CommunityExecutive的manager
class MarketingManager extends HiringManager {
  @override
  Interviewer _makeInterviewer() {
    return CommunityExecutive();
  }
}

/*
02-工厂方法
目的
建立 Interface ，但给 SubClass 自行决定要如何初始化。将初始化延至 SubClass。

何时使用
当 SubClass 有共用的属性，但需要在 Runtime 时动态决定一些事情。即 Client 不需知道 SubClass 的 Detail。
 */
void main(List<String> args) {
  var devManager = DevelopmentManager();
  var makManager = MarketingManager();

  print("""
    // Create a development manager.
    ${devManager.takeInterview()}
    
    // Create a marketing manager.
    ${makManager.takeInterview()}
    """);

  // FactoryMethod👇👇👇
  // // Create a development manager.
  // What is Design Pattern ?

  // // Create a marketing manager.
  // What is Community Building ?
}
