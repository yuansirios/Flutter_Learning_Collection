// 抽象类
abstract class Door {
  String description();
}

// 木门
class WoodenDoor implements Door {
  @override
  String description() => "This is a wooden door.";
}

// 铁门
class IronDoor implements Door {
  @override
  String description() => "This is a iron door.";
}

// 维修工人
abstract class DoorExpert {
  String description();
  String fix();
}

// 木门维修工
class Carpenter extends DoorExpert {
  @override
  String description() => "I am a carpenter.";

  @override
  String fix() => "I am fixing wooden doors.";
}

// 铁门维修工
class Welder implements DoorExpert {
  @override
  String description() => "I am a welder.";

  @override
  String fix() => "I am fixing iron doors.";
}

// 创建一个工厂，将不同种类的门跟维修工对应起来
// 此处仅创建接口，具体由子类实现
abstract class DoorFactory {
  // 创建门
  Door makeDoor();
  // 召唤维修工
  DoorExpert callDoorExpert();
}

class WoodenDoorFactory implements DoorFactory {
  @override
  Door makeDoor() => WoodenDoor();

  @override
  DoorExpert callDoorExpert() => Carpenter();
}

class IronDoorFactory implements DoorFactory {
  @override
  Door makeDoor() => IronDoor();

  @override
  DoorExpert callDoorExpert() => Welder();
}

/*
抽象工厂

目的
建立一个 Interface 囊括所有相关实体的家族，且不暴露他们的 Class 。

何时使用
当有一些相依实体需要维护，且产生逻辑有一定的复杂程度时，将依赖关系放到抽象工厂中。
 */
void main(List<String> args) {
  var woodenDoorFactory = WoodenDoorFactory();
  var woodenDoor = woodenDoorFactory.makeDoor();
  var carpenter = woodenDoorFactory.callDoorExpert();

  var ironDoorFactory = IronDoorFactory();
  var ironDoor = ironDoorFactory.makeDoor();
  var welder = ironDoorFactory.callDoorExpert();

  print("""
    ${woodenDoor.description()}
    ${carpenter.description()}
    ${carpenter.fix()}
    ------
    ${ironDoor.description()}
    ${welder.description()}
    ${welder.fix()}
    """);

  // AbstractFactory👇👇👇
  // This is a wooden door.
  // I am a carpenter.
  // I am fixing wooden doors.
  // ------
  // This is a iron door.
  // I am a welder.
  // I am fixing iron doors.
}
