// 计算机底层api，供调用
class Computer {
  String getElectricShock() => "Ouch!";

  String makeSound() => "Beep beep!";

  String showLoadingScreen() => "Loading...";

  String bam() => "Ready to be used!";

  String closeEverything() => "Bup bup bup buzzzz!";

  String sooth() => "ZZzzz";

  String pullCurrent() => "Haaah!";
}

// 高阶计算机操作类，封装低阶接口调用过程，方便使用
class ComputerFacade {
  Computer computer;

  ComputerFacade(this.computer);

  String turnOn() => """
${computer.getElectricShock()}
${computer.makeSound()}
${computer.showLoadingScreen()}
${computer.bam()}
    """;

  String turnOff() => """
${computer.closeEverything()}
${computer.pullCurrent()}
${computer.sooth()}
  """;
}

/*
门面模式

目的
创造一个 High-Level 的接口，来让底下的系统更好被使用。

何时使用
当你有一堆小的子系统，要一个一个操作很繁杂的时候。
 */
void main(List<String> args) {
  Computer computer = Computer();
  ComputerFacade facade = ComputerFacade(computer);

  print("""
        Now turn on the computer.
        ${facade.turnOn()}
    
        Turn off the computer.
        ${facade.turnOff()}
    """);

  // Facade👇👇👇
  // // Now turn on the computer.
  // Ouch!
  // Beep beep!
  // Loading...
  // Ready to be used!

  // // Turn off the computer.
  // Bup bup bup buzzzz!
  // Haaah!
  // ZZzzz
}
