// 抽象出员工公共属性
abstract class Employee {
  String name;
  double salary;

  Employee(this.name, this.salary);
}

class Developer extends Employee {
  Developer(String name, double salary) : super(name, salary);
}

class Designer extends Employee {
  Designer(String name, double salary) : super(name, salary);
}

// 为了方便管理员工，将员工实体们统一放到 company 中进行管理
class Company {
  //所有员工
  List<Employee> _employees = [];

  void addEmployee(Employee employee) {
    _employees.add(employee);
  }

  double getNetSalaries() {
    return _employees
        .map((employee) => employee.salary) //转员工工资map
        .fold(0, (net, salary) => net + salary);
    //net：上一个value，salary：当前的工资
    //(net, salary) => net + salary <--> value = pre + now
  }
}

/*
组合模式

目的
将相同 interface 但不同功能的实体，集中到树状结构来管理。

何时使用
当有一群类似的实体，想要集中管理时。
 */
void main(List<String> args) {
  var jay = Developer("Jay Wang", 10000);
  var wei = Designer("Wei Chang", 1);

  var jayIsBetterCompany = Company();
  jayIsBetterCompany.addEmployee(jay);
  jayIsBetterCompany.addEmployee(wei);

  print(" Net salary is ${jayIsBetterCompany.getNetSalaries()}");

  // Composite👇👇👇
  // Net salary is 10001.0
}
